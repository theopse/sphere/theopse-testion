#include "erl_nif.h"
#include <stdlib.h>

ErlNifResourceType *term_t = NULL;
typedef struct {
    ERL_NIF_TERM* item;
} Term;

ERL_NIF_TERM is_list(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]) {
    Term *item;
    size_t pt;
    if (!term_t) {
        if (!enif_get_uint64(env, argv[1], &pt)) {
            return enif_make_badarg(env);
        }
        term_t = (ErlNifResourceType *)(NULL + pt);
    }
    if (!enif_get_resource(env, argv[0], term_t, (void **)&item)) {
        return enif_make_badarg(env);
    }
    int result = enif_is_list(env, *(item->item));
    return enif_make_atom(env, result ? "true" : "false");
}

static ErlNifFunc nif_funcs[] = {
    {"p_list?", 2, is_list}
};

ERL_NIF_INIT(Elixir.Theopse.Testion.Nifres.Term2, nif_funcs, NULL, NULL, NULL, NULL);