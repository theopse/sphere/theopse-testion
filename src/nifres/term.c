#include "erl_nif.h"
#include <memory.h>
#include <stdio.h>

ErlNifResourceType *term_t = NULL;
void destruct(ErlNifEnv *env, void *obj);
typedef struct {
    ERL_NIF_TERM* item;
} Term;

static int init(ErlNifEnv *env, void **priv_data, ERL_NIF_TERM load_info)
{
    term_t = enif_open_resource_type(env, NULL, "Theopse.Testion.Nifres.Term", destruct, ERL_NIF_RT_CREATE, NULL);
    return 0;
}

ERL_NIF_TERM get_type(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]) {
    return enif_make_uint64(env, (void*)term_t - NULL);
}

ERL_NIF_TERM new(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]) {
    Term *item = enif_alloc_resource(term_t, sizeof(Term));
    item->item = enif_alloc(sizeof(ERL_NIF_TERM));
    *(item->item) = argv[0];
    ERL_NIF_TERM ref = enif_make_resource(env, item);
    enif_release_resource(item);
    return ref;
}

ERL_NIF_TERM get_item(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]) {
    Term *item;
    if (!enif_get_resource(env, argv[0], term_t, (void **)&item)) {
        return enif_make_badarg(env);
    }
    ERL_NIF_TERM p = *(item->item);
    // enif_release_resource(item);
    return p;
}

ERL_NIF_TERM set_item(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]) {
    Term *item;
    if (!enif_get_resource(env, argv[0], term_t, (void **)&item)) {
        return enif_make_badarg(env);
    }
    memcpy(item->item, argv + 1, sizeof(ERL_NIF_TERM));
    // *(item->item) = argv[1];
    // enif_release_resource(item);
    return enif_make_atom(env, "true");
}


void destruct(ErlNifEnv *env, void *obj) {
    Term *obj_true = (Term *)obj;
    enif_free(obj_true->item);
    printf("%p has been destructed.\n", obj);
}

static ErlNifFunc nif_funcs[] = {
    {"get_type", 0, get_type},
    {"p_new", 1, new},
    {"p_get_item", 1, get_item},
    {"p_set_item", 2, set_item},
};

ERL_NIF_INIT(Elixir.Theopse.Testion.Nifres.Term, nif_funcs, init, NULL, NULL, NULL);