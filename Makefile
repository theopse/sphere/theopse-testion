ERLANG_PATH = $(shell erl -eval 'io:format("~s", [lists:concat([code:root_dir(), "/erts-", erlang:system_info(version), "/include"])])' -s init stop -noshell)

all:
	mkdir -p priv/nifres
	zigc -fpic -shared -o priv/nifres/term.so src/nifres/term.c -I"$(ERLANG_PATH)"
	zigc -fpic -shared -o priv/nifres/term2.so src/nifres/term2.c -I"$(ERLANG_PATH)"

clean:
	rm  -r "priv/nifres/term.so"
	rm  -r "priv/nifres/term2.so"