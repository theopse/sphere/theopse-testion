
defmodule Theopse.Testion.Nifres.Term do
  @on_load :__init__
  use Theopse.Core
  use Theopse.Testion.Nifres

  alias Theopse.Testion.Nifres.Term

  defstruct ref: nil

  defp __init__() do
    ok() = :erlang.load_nif('priv/nifres/term', 0)
  end

  def get_ref(%Term{ref: ref}), do: ref

  def get_type(), do: exit(:nif_not_loaded)

  def get_item(%Term{ref: ref}), do: ok(p_get_item(ref))
  def get_item!(term) do
    case get_item(term) do
      ok(item) -> item
      _ -> nil
    end
  end
  def set_item(term = %Term{ref: ref}, item) do
    if p_set_item(ref, item) do
      ok(term)
    else
      error()
    end
  end

  def new(opts \\ [])
  def new([item | _]), do: ok(%Term{ref: p_new(item)})
  def new(_), do: ok(%Term{ref: p_new(nil)})

  # def alive?(%Term{ref: ref}), do: p_alive(ref)

  # def free(%Term{ref: ref}), do: p_free(ref)

  defp p_new(_item), do: exit(:nif_not_loaded)
  defp p_get_item(_ref), do: exit(:nif_not_loaded)
  defp p_set_item(_ref, _item), do: exit(:nif_not_loaded)
  # defp p_alive(_ref), do: exit(:nif_not_loaded)
  # defp p_free(_ref), do: exit(:nif_not_loaded)

  def test() do
    c = for n <- 1..10000, do: new([n]) |> then(fn {:ok, item} -> item end)
    Enum.map(c, fn a ->
      ok(b) = get_item(a)
      set_item(a, b + 1)
      get_item(a)
    end)
    |> Enum.each(&IO.inspect/1)
    :ok
  end

  defimpl String.Chars do
    def to_string(term), do: "#Theopse.Term<#{Theopse.Testion.Nifres.Term.get_item(term) |> then(fn ok(item) -> item end) |> String.Chars.to_string()}>"
  end

  defimpl Inspect do
    import Inspect.Algebra

    def inspect(term, opts) do
      concat([
        "#Theopse.Term<",
        Inspect.inspect(Theopse.Testion.Nifres.Term.get_item(term) |> then(fn ok(item) -> item end), opts),
        ">"
      ])
    end
  end

end
