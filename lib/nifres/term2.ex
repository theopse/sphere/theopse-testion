
defmodule Theopse.Testion.Nifres.Term2 do

  @on_load :__init__
  alias Theopse.Testion.Nifres.Term

  defp __init__() do
    :ok = :erlang.load_nif('priv/nifres/term2', 0)
  end

  def list?(%Term{ref: ref}), do: p_list?(ref, Term.get_type)
  defp p_list?(_item, _type), do: exit(:nif_not_loaded)
end
