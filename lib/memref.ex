
defmodule Theopse.Testion.Memref do
  @callback get_type() :: integer
  @callback get_ref(term) :: reference

  def get_type(%{__struct__: module}), do: module.get_type()
  def get_ref(term = %{__struct__: module}), do: module.get_ref(term)

  defmacro __using__(_opts \\ []) do
    quote do
      @behaviour Theopse.Testion.Memref
    end
  end

end
