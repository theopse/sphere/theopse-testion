
defmodule Theopse.Testion.Nifres do
  @callback new(list(term)) :: {:ok, term} | {:error, any}
  # @callback free(term) :: :ok | {:error, any}

  def new(module, opts \\ [])
  def new(%{__struct__: module}, opts), do: module.new(opts)
  def new(module, opts) when is_atom(module), do: module.new(opts)

  # def alive?(term = %{__struct__: module}), do: module.alive?(term)

  # def free(term = %{__struct__: module}), do: module.new(term)

  defmacro __using__(_opts \\ []) do
    quote do
      @behaviour Theopse.Testion.Memref
      @behaviour Theopse.Testion.Nifres
    end
  end
end
