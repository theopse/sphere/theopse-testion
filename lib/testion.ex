defmodule Testion do
  @moduledoc """
  Documentation for `Testion`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Testion.hello()
      :world

  """
  def hello do
    :world
  end
end
